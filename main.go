// Generate column headings in spreadsheet style (at least for Google
// Sheets).  Go through the alphabet, then the alphabet prefixed with
// "A", then prefixed with "B", etc.
package main

import (
	"flag"
	"fmt"
	"os"
)

func main() {
	var (
		letters = []string{
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
			"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		}

		// For doing a certain number of headings: when you know
		// you have, say, 129 columns.
		needed int
		done   = 0

		// For stopping at a certain heading: when you know the
		// last column you need a label for is, say, "DY".
		last string

		n = flag.Int("n", 0, "number of headings needed")
		l = flag.String("l", "", "last heading needed")

		method string
	)
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Must set either -n or -l.\n")
		fmt.Fprintf(os.Stderr, "-l takes precedence if both are set.\n\n")
		flag.PrintDefaults()
		os.Exit(1)
	}
	flag.Parse()

	if *n > 0 {
		needed = *n
		method = "number"
	}

	if *l != "" {
		last = *l
		method = "string"
	}

	if method == "" {
		flag.Usage()
	}

Outerloop:
	for i := 0; i <= len(letters); i++ {
		pre := ""
		if i > 0 {
			pre = letters[i-1]
		}
		for _, l := range letters {
			label := pre + l
			fmt.Println(label)
			done++

			switch method {
			case "number":
				if done == needed {
					break Outerloop
				}
			case "string":
				if label == last {
					break Outerloop
				}
			default:
				// shouldn't reach
				fmt.Fprintf(os.Stderr, "shouldn't reach\n")
				os.Exit(2)
			}

		}
	}
}
